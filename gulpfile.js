var gulp = require('gulp'),
    connect = require('gulp-connect'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    port = process.env.port | 5000;

gulp.task('browserify', function() {
    //gulp.src('./app/js/main.js')
    gulp.src('./app/js/restful.js')  //new restful service client
        .pipe(browserify({
            transform: 'reactify'
        }))
        /*.pipe(uglify())*/
        .pipe(gulp.dest('./dist/js'))
});

/*gulp.task('build', function() {
    gulp.src(['./dist/js/*.js'])
        .pipe(uglify(main.min.js))
        .pipe(gulp.dest('./dist/js'))
})*/

//live reload
gulp.task('connect', function() {
    connect.server({
        port: port,
        livereload: true
    })
})

//reload js,html,etc
gulp.task('js', function() {
    gulp.src('./dist/**/*.js')
        .pipe(connect.reload())
})

gulp.task('html', function() {
    gulp.src('./app/**/*.html')
        .pipe(connect.reload())
})

gulp.task('watch', function() {
    gulp.watch('./dist/**/*.js', ['js']);
    gulp.watch('./app/**/*.html', ['html']);
    gulp.watch('./app/**/*.js', ['browserify']);
})

gulp.task('default', ['browserify']);

gulp.task('serve', ['browserify', /*'build',*/ 'connect', 'watch']);