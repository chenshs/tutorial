var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getAllUrls',
    'fillUrlNParams',
    'updateUrl',
    'addParameter',
    'removeParameter',
    'updateParamKey',
    'updateParamValue',
    'setActiveUrl',
    'updateRest',
    'getRestResult',
    'resetJsonOutput',
    'addNewUrl',
    'setActiveMenu',
    'selectNode',
    'removeUrl'
])