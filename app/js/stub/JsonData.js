var JsonData = {};

JsonData.svgTreeData = {
    "name": "PFSubAcctTrig",
    "children": [{
            "name": "Update History Table",
            "children": [{
                "name": "杭州"
            }, {
                "name": "宁波"
            }, {
                "name": "温州"
            }, {
                "name": "绍兴"
            }]
        },

        {
            "name": "Call Extra Store procedure",
            "children": [{
                "name": "桂林",
                "children": [{
                    "name": "秀峰区"
                }, {
                    "name": "叠彩区"
                }, {
                    "name": "象山区"
                }, {
                    "name": "七星区"
                }]
            }, {
                "name": "南宁"
            }, {
                "name": "柳州"
            }, {
                "name": "防城港"
            }]
        },

        {
            "name": "Update Bus Context fields",
            "children": [{
                "name": "哈尔滨"
            }, {
                "name": "齐齐哈尔"
            }, {
                "name": "牡丹江"
            }, {
                "name": "大庆"
            }]
        },

        {
            "name": "Check RefCode logic",
            "children": [{
                "name": "乌鲁木齐"
            }, {
                "name": "克拉玛依"
            }, {
                "name": "吐鲁番"
            }, {
                "name": "哈密"
            }]
        }
    ]
};

JsonData.skeTreeData = {
    title: "AccountsData",
    key: 1,
    isSelected: false,
    childNodes: [{
        title: "Trigger",
        key: 3,
        isSelected: false,
        childNodes: [{
            title: "PFSubAcctTrig",
            key: 4,
            isSelected: false

        }, {
            key: 6,
            title: "PFSubEqTrig",
            isSelected: false
        }]
    }]
};

module.exports = JsonData;