var React = require('react');
var Reflux = require('reflux');
var TreeNode = require('./TreeNode.js');
var JsonData = require('../stub/JsonData.js');
var Action = require('../actions/Action.js');
var NavTreeStore = require('../stores/NavTreeStore.js');

module.exports = React.createClass({
    mixins: [Reflux.connect(NavTreeStore, 'selected')],
    getInitialState: function() {
        return {
            selected: ''
        }
    },
    componentDidMount: function() {
        Action.selectNode(-1);
    },
    handleStatus: function(json) {

        console.log(this.state.selected + ':' + json.key);
        if (this.state.selected == json.key) {
            json.isSelected = true;
            console.log(this.state.selected + ':selected true:' + json.key);
        } else {

            json.isSelected = false;
        }
        if (json.childNodes && json.childNodes.length > 0) {
            for (var i = 0; i < json.childNodes.length; i++) {
                this.handleStatus(json.childNodes[i]);
            }

        }
    },

    handleNodeClick: function(node) {
        Action.selectNode(node.key);
        this.props.renderSVG(node);
    },

    render: function() {
        console.log('select:' + this.state.selected);
        var node = JsonData.skeTreeData;

        if (this.state.selected != '') {
            this.handleStatus(node);
        }
        return <TreeNode node = {node} handleSel = {this.handleNodeClick}/>
    }
})