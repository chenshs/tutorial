var React = require('react');
var AddOns = require('react/addons');

var TreeNode = React.createClass({
    getInitialState: function() {
        return {
            visible: true
        };
    },
    render: function() {
        var childNodes;

        var nodeClick = this.props.handleSel;
        if (this.props.node.childNodes != null) {
            childNodes = this.props.node.childNodes.map(function(node, index) {
                return <li key={index}><TreeNode node={node} handleSel={nodeClick}/></li>
            });

        }

        var style;
        if (!this.state.visible) {
            style = {
                display: "none"
            };
        }
        var nodeStyle;
        if (this.props.node.isSelected) {
            console.log('find is selected');
            nodeStyle = {
                backgroundColor: "#ccc",
                color: 'red',
                cursor: 'pointer'

            }
        } else {
            nodeStyle = {

                cursor: 'pointer'
            }
        }
        var icon;
        if (this.props.node.childNodes && this.props.node.childNodes.length > 0) {
            if (this.state.visible) {
                icon = (<img onClick={this.toggle} src='./dist/images/open.ico' className="img-treeNode"/>);
            } else {
                icon = (<img onClick={this.toggle} src='./dist/images/close.ico' className="img-treeNode"/>);
            }

        } else {
            icon = (<img onClick={this.toggle} src='./dist/images/leaf.ico' className="img-treeleaf"/>);
        }
        return (
            <div> <span>{icon}<h5  onClick={this.select} style={nodeStyle}>
          {this.props.node.title}
        </h5></span>
        <ul style={style}>
          {childNodes}
        </ul>
      </div>
        );
    },
    toggle: function() {
        this.setState({
            visible: !this.state.visible
        });
    },
    select: function() {
        this.props.handleSel({
            key: this.props.node.key,
            title: this.props.node.title
        });
    }
})

module.exports = TreeNode;