var React = require('react');

module.exports = React.createClass({
    getDefaultProps: function() {
        return {
            actType: 'Fund',
            actName: 'Testing fund',
            actMnemonic: '512322',
            pfId: '115232'
        }
    },

    render: function() {
        console.log('render detail items..');
        return ( <div className = "page-header">
            <h1> {this.props.actType}</h1>
            <small> Account Name: {this.props.actName}</small><br/>
            <small> Account Mnemonic: {this.props.actMnemonic}</small><br/>
            <small> Account Id: {this.props.pfId}< /small><br/>
            </div>
        )
    }
})