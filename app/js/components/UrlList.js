var React = require('react');
var UrlItem = require('./UrlItem');
var UrlListStore = require('../stores/UrlListStore');
var Action = require('../actions/Action.js');
var Reflux = require('reflux');

module.exports = React.createClass({
    mixins: [Reflux.connect(UrlListStore, 'urls')],
    getInitialState: function() {
        return {
            urls: {
                urllist: [],
                activeUrl: -1
            }
        }
    },
    componentDidMount: function() {
        Action.getAllUrls();
    },
    render: function() {
        var licomp = this.state.urls.urllist.map(function(listitem, index) {
           
            var isActive = "false";
            if (this.state.urls.activeUrl == index) {
                isActive = "true";
            }
            return <UrlItem key={index} indx={index} listName={listitem.urlName} isActive={isActive} />
        }.bind(this));

        // console.log('urllist render:' + licomp + ';type:' + Array.isArray(licomp));
        return (<div className="panel-body">
            <ul className="nav nav-pills nav-stacked">
                {licomp}
            </ul> 
                 </div>);
    }
})