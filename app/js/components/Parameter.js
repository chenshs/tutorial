var React = require('react');
var Action = require('../actions/Action.js');

module.exports = React.createClass({

    removeParam: function() {
        this.props.removeParam(this.props.indx);
    },

    handleKeyInput: function(e) {
        var val = e.target.value;
        Action.updateParamKey(this.props.indx, val);

    },
    handleValueInput: function(e) {
        var val = e.target.value;
        Action.updateParamValue(this.props.indx, val);
    },
    render: function() {
        var divStyle = {
            width: '40%'
        };
        return (<form className="form-inline f-padding">
                    <div className="form-group" style={divStyle}>
                        <span>Key:</span>
                        <input type="text" className="form-control" value={this.props.k} placeholder="key..." 
                           onChange={this.handleKeyInput} />
                    </div>
                    <div className="form-group" style={divStyle}>
                        <span>Value:</span>
                        <input type="text" className="form-control" value={this.props.v} placeholder="value... "
                          onChange={this.handleValueInput} />
                        
                        <img  src='./dist/images/delete.ico' onClick={this.removeParam}/>
                    </div>
                </form>)
    }
})