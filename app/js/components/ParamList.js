var React = require('react');
var Parameter = require('./Parameter.js');
var ParamStore = require('../stores/ParamStore.js');
var Reflux = require('reflux');
var Action = require('../actions/Action.js');

module.exports = React.createClass({
    mixins: [Reflux.connect(ParamStore, 'urlall')],
    getInitialState: function() {
        return {
            urlall: {
                url: '',
                params: [{
                    key: '',
                    value: ''
                }]
            }

        }
    },
    addParams: function() {
        Action.addParameter();
    },

    removeParam: function(indx) {
        Action.removeParameter(indx);
    },
    handleInput: function(e) {
        Action.updateUrl(e.target.value);
    },
    handleRestfulRequest: function() {
        var rest = this.state.urlall.url;
        if (rest == null || rest.length == 0) {
            return;
        }
        this.state.urlall.params.map(function(itm, indx) {
            if (indx == 0) {
                rest = rest + '?' + itm.key + '=' + itm.value;
            } else {
                rest = rest + '&' + itm.key + '=' + itm.value;
            }

        });
        Action.updateRest(rest);
        Action.getRestResult(rest);
    },
    handleRestRun: function() {
        var rest = this.state.urlall.url;
        if (rest == null || rest.length == 0) {
            return;
        }
        this.state.urlall.params.map(function(itm, indx) {
            if (indx == 0) {
                rest = rest + '?' + itm.key + '=' + itm.value;
            } else {
                rest = rest + '&' + itm.key + '=' + itm.value;
            }

        });
        Action.getRestResult(rest);
    },
    render: function() {
        var saveBtn = <div className="f-padding">
                    <button className="btn btn-success" type="button" onClick={this.handleRestRun}>Run</button>
                    <button className="btn btn-success buttonSpace" type="button" onClick={this.handleRestfulRequest}>Save & Run</button>                    
                </div>;
        var url = this.state.urlall.url;
        var pms = this.state.urlall.params;
        console.log("rendering paramslist:" + url);

        var paramslist = pms.map(function(p, index) {
            return <Parameter key={index} indx={index} k={p.key} v={p.value} removeParam={this.removeParam}/>;
        }.bind(this));
        return (<div> <form className="form-horizontal">
                    <div className="col-sm-1  s-padding">
                        <span>URL:</span>
                    </div>
                    <div className="input-group col-sm-11">
                        <input type="text" className="form-control" value= {url} placeholder="Restful service URL..." onChange={this.handleInput}/>
                        <span className="input-group-btn">
                         <button className="btn btn-success" type="button" onClick={this.addParams}>Add Parameter</button>
                        </span>
                    </div>
                </form>
                {paramslist} 
                {saveBtn}
                </div>)
    }
})