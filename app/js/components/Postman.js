var React = require("react");
var UrlList = require('./UrlList.js');
var ParamList = require('./ParamList.js');
var JsonOutput = require('./JsonOutput.js');
var Action = require('../actions/Action.js');

module.exports = React.createClass({

    handleAddNew: function(e) {
        console.log('add new url');
        Action.addNewUrl();
    },
    render: function() {

        return (<div className="main container c-padding">
        <div className="row">
            <div className="col-md-4 ">
                <div className="panel panel-default">
                    <div className="panel-heading">URL List<span className="glyphicon glyphicon-plus-sign headericon iconcursor" 
                      aria-hidden="true" title="Create New" onClick={this.handleAddNew}></span></div>                   
                       <UrlList />                
                </div>
            </div>
            <div className="col-md-8">
           
                <ParamList  />            

                <JsonOutput />
            </div>
        </div>
    </div>);
    }
})