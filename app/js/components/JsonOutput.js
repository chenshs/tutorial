var React = require('react');
var Reflux = require('reflux');
var JsonOutputStore = require('../stores/JsonOutputStore.js');

module.exports = React.createClass({
    mixins: [Reflux.connect(JsonOutputStore, 'jst')],
    getInitialState: function() {
        return {
            jst: {
                inprocess: false,
                jsonText: 'The json output comes here.'
            }
        }
    },
    syntaxHighlight: function(json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
            function(match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
    },
    render: function() {
        console.log('render json output...');
        if (this.state.jst.inprocess) {
            return (<div className="col-sm-11 r-padding">
                    <img src="images/loading.gif"></img>
                </div>);
        } else {
            return (<div className="col-sm-11 r-padding" >
                   <pre dangerouslySetInnerHTML=
                {{__html:this.syntaxHighlight(this.state.jst.jsonText)}} />
                </div>);
        }

    }
})