var React = require("react");
var DetailItem = require('./DetailItem.js')

module.exports = React.createClass({
    getInitialState: function() {
        return {
            beforeSearch: true,
            items: []
        }
    },

    handleSearch: function(e) {
        e.preventDefault();
        this.setState({
            beforeSearch: false
        });
        this.setState({
            items: [{
                actType: 'Client',
                actName: 'Goldman investment',
                actMnemonic: '522782733',
                pfId: '116754'
            }, {
                actType: 'Fund',
                actName: 'Blackrock investment',
                actMnemonic: '522782733',
                pfId: '116754'
            }]
        })
    },

    render: function() {
       // console.log('render main items..');
        var itemsComp = this.state.items.map(function(item) {
            console.log(item.actType);
            return <DetailItem actType = {item.actType} actName = {item.actName}
                  actMnemonic = {item.actMnemonic} pfId = {item.pfId} />
        }.bind(this));
        var styleObject = {};
        if (this.state.beforeSearch) {
            styleObject = {
                padding: '150px 0'
            }
        } else {
            styleObject = {
                padding: '10px 0'
            }
        }
        return (<div className = "main container"  style={ styleObject}>
            <div className = "row">
            <div className = "col-md-8 col-md-offset-2">
            <form onSubmit= {this.handleSearch} >
            <div className = "input-group">
            <input type = "text"
            className = "form-control"
            placeholder = "Search for......" />
            <span className = "input-group-btn" >
            <button type="submit" className = "btn btn-success" > Go! </button> </span>
             </div> 
            </form>
            {itemsComp }
            </div >          
            </div> </div>)
    }
})