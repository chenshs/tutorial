var React = require('react');
var Action = require('../actions/Action.js');

module.exports = React.createClass({
    getRestResult: function() {
        //console.log("get restful  of " + this.props.listName);
        Action.fillUrlNParams(this.props.listName);
        Action.resetJsonOutput();
        Action.setActiveUrl(this.props.indx);
    },
    removeItem: function() {
        Action.removeUrl(this.props.indx);
        Action.setActiveUrl(-1);
    },
    render: function() {
        var imgStyle = {
            float: 'right',
            marginTop: '-28px',
            marginRight: '-10px',
            cursor: 'pointer'

        }
        var activeStyle = this.props.isActive === 'true' ? 'active' : '';
        return (<li role="presentation" title={this.props.listName} className={activeStyle}>
            <a className="textWrap" href="#" onClick={this.getRestResult}>{this.props.listName}</a>            
            <img style={imgStyle} src='./dist/images/delete.ico' onClick={this.removeItem}/></li>);
    }
})