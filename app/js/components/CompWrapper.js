var React = require('react');
var DataFlow = require('./DataFlow.js');
var Postman = require('./Postman.js');
var MenuStore = require('../stores/MenuStore.js');
var Menu = require('./Menu.js');
var Reflux = require('reflux');

module.exports = React.createClass({
        mixins: [Reflux.connect(MenuStore, 'activeMenu')],
        getInitialState: function() {
            return {
                activeMenu: 1
            }
        },
        render: function() {
            console.log('comp wrapper;');
            var content = null;
            if (this.state.activeMenu == 1) {
                content = <Postman />;
            } else if (this.state.activeMenu == 2) {
                content = <DataFlow />;
            }

            return (
                <div> 
         <div className="header_top">
            <div className="container">
               <div className="header_top-box">
                 <div className="cssmenu">
                    <ul>
                        <li><a href="login.html">Log In</a></li>
                        <li><a href="register.html">Sign Up</a></li>
                    </ul>
                 </div>
                        <div className="clearfix"></div>
               </div>
            </div>
         </div>

         <div className = "header_bottom">
            <div className="container">
              <div className="header_bottom-box">
                <div className="header_bottom_left">
                    <div className="logo">
                        <a href="index.html"><img src="dist/images/logo.jpg" alt="" /></a>
                    </div>
                    <div className="clearfix"> </div>
                </div>
                <div className="header_bottom_right">
                    <div className="search">
                        <input type="text" value="Search..." />
                        <input type="submit" value=""/>
                    </div>
                </div>
                <div className="clearfix"> </div>
            </div>
          </div> 
          </div>

          <div className = "menu">
              <div className="container">
                <div className="menu_box">
                    <Menu activeMenu={this.state.activeMenu}/>
                </div>
              </div> 
           </div>{content} 
         </div>)
        }
    }

)