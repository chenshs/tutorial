var React = require('react');
var Reflux = require('reflux');
var Action = require('../actions/Action.js');

module.exports = React.createClass({
    getDefaultProps: function() {
        return {
            activeMenu: 2
        }

    },
    handlePostClick: function(e) {
        e.preventDefault();
        Action.setActiveMenu(1);

    },
    handleDataClick: function(e) {
        console.log('click 2');
        e.preventDefault();
        Action.setActiveMenu(2);

    },
    render: function() {
        console.log(this.props.activeMenu);
        if (this.props.activeMenu == 1) {
            return (<ul className="megamenu skyblue">
                        <li className="active grid"><a className="color2" href="#" onClick={this.handlePostClick}>Postman</a></li>
                        <li><a className="color4" href="#" onClick={this.handleDataClick}>Data Flow</a></li>
                    </ul>)
        } else {
            return (<ul className="megamenu skyblue">
                        <li><a className="color2" href="#" onClick={this.handlePostClick}>Postman</a></li>
                        <li className="active grid"><a className="color4" href="#" onClick={this.handleDataClick}>Data Flow</a></li>
                    </ul>)
        }

    }
})