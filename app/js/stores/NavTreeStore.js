var Reflux = require('reflux');
var Action = require('../actions/Action.js');

module.exports = Reflux.createStore({
    listenables: [Action],
    onSelectNode: function(item) {
        console.log('select node');
        this.trigger(item);
    }
})