var Reflux = require('reflux');
var Action = require('../actions/Action.js');

module.exports = Reflux.createStore({
    listenables: [Action],
    onSetActiveMenu: function(item) {
        console.log('onset active menu');
        this.trigger(item);
    }
})