var Reflux = require('reflux');
var Action = require('../actions/Action.js');

module.exports = Reflux.createStore({
    listenables: [Action],
    fullURL: {
        url: '',
        params: [{
            key: '',
            value: ''
        }]
    },
    onFillUrlNParams: function(urlstr) {
        var result = urlstr.split('?');
        var pms = [];
        if (result.length > 1) {
            var pmstr = result[1].split('&');
            for (var x = 0; x < pmstr.length; x++) {
                var pm = pmstr[x].split('=');
                if (pm.length > 1) {
                    pms.push({
                        key: pm[0],
                        value: pm[1]
                    })

                }

            }

        }
        this.fullURL.params = pms;
        this.fullURL.url = result[0];

        console.log('onFillUrlNParams: ' + this.fullURL.url);
        this.trigger(this.fullURL);

    },

    onUpdateUrl: function(newurl) {
        this.fullURL.url = newurl;
        this.trigger(this.fullURL);
    },

    onAddParameter: function() {
        this.fullURL.params.push({
            key: '',
            value: ''
        });
        this.trigger(this.fullURL);
    },

    onRemoveParameter: function(indx) {
        this.fullURL.params.splice(indx, 1);
        this.trigger(this.fullURL);
    },

    onUpdateParamKey: function(indx, kval) {
        this.fullURL.params[indx].key = kval;
        this.trigger(this.fullURL);
    },

    onUpdateParamValue: function(indx, vval) {
        this.fullURL.params[indx].value = vval;
        this.trigger(this.fullURL);
    }

})