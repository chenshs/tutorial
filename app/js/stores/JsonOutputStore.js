var Reflux = require('reflux');
var Action = require('../actions/Action.js');
var $ = require('jquery');

module.exports = Reflux.createStore({
    listenables: [Action],
    jst: {
        inprocess: false,
        jsonText: 'The json output comes here.'
    },
    onGetRestResult: function(urlname) {
        console.log('getrestResult...');
        this.jst.inprocess = true;
        this.trigger(this.jst);
        $.ajax({
            url: '/getRest',
            data: {
                urlstr: urlname
            },
            dataType: 'json',
            success: function(data) {
                console.log(data);
                this.jst.inprocess = false;
                this.jst.jsonText = JSON.stringify(data, undefined, 4);
                this.trigger(this.jst);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(err.toString());
                this.jst.inprocess = false;
                this.jst.jsonText = 'Failed to get restful result';
                this.trigger(this.jst);
            }.bind(this)
        });
    },
    onResetJsonOutput: function() {
        this.trigger({
            inprocess: false,
            jsonText: 'Json output comes here'
        });
    }
})