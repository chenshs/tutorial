var Reflux = require('reflux');
var Action = require('../actions/Action.js');
var $ = require('jquery');
module.exports = Reflux.createStore({
    //items: [],
    urls: {
        urllist: [],
        activeUrl: -1
    },
    listenables: [Action],
    onGetAllUrls: function() {

        $.ajax({
            url: '/urls',
            dataType: 'json',
            success: function(data) {
                //JSONArray jsonArray = JSONArray.fromObject(data);
                console.log(data);
                this.urls.urllist = data;
                this.trigger(this.urls);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(err.toString());
                var items = [{
                    key: '1',
                    urlName: 'https://pfsvlnx1p.nam.nsroot.net:10001/CuesService/getUsers?x=y'
                }, {
                    key: '2',
                    urlName: 'https://pfsvlnx2p.nam.nsroot.net:10001/Window/getlist'
                }, {
                    key: '3',
                    urlName: 'https://pfsvlnx3p.nam.nsroot.net:10001/Test/getTestCases'
                }];
                this.urls.urllist = items;
                this.urls.activeUrl = -1;
                this.trigger(this.urls);
            }.bind(this)
        });

    },
    onSetActiveUrl: function(indx) {
        console.log('set Active :' + indx);
        this.urls.activeUrl = indx;
        this.trigger(this.urls);
    },
    onUpdateRest: function(newrst) {
        console.log('new rest:' + newrst);
        this.urls.urllist[this.urls.activeUrl].urlName = newrst;
        console.log(this.urls.urllist[this.urls.activeUrl].urlName);
        this.trigger(this.urls);
    },
    onAddNewUrl: function() {
        this.urls.urllist.push({
            key: this.urls.urllist.length,
            urlName: 'New Url..'
        });
        this.trigger(this.urls);
    },
    onRemoveUrl: function(indx) {
        this.urls.urllist.splice(indx, 1);
        this.trigger(this.urls);
    }
})